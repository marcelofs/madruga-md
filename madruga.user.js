// ==UserScript==
// @name			Madruga
// @author			nW0lf
// @namespace		https://madruga.firebaseapp.com
// @description		Carrega as ordens do Ministério da Defesa do eBR na página principal do eRepublik
// @include			/^http(s)?://(www\.)?erepublik\.com/[a-z]{2}(\?(view|unit)Post=(\d)*)?$/
// @version			0.7.4
// @updateURL		https://madruga.firebaseapp.com/madruga.min.user.js
// @downloadURL		https://madruga.firebaseapp.com/madruga.min.user.js
// @grant			GM_xmlhttpRequest
// @grant			GM_getValue
// @grant			GM_setValue
// @grant			GM_addStyle
// @connect 		goo.gl
// @connect 		docs.google.com
// @connect 		madruga.firebaseio.com
// @connect 		api.ipify.org
// @connect 		www.erepublik.com
// @connect 		self 
// @connect 		*
// @require			https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js
// @require			https://raw.githubusercontent.com/mholt/PapaParse/master/papaparse.min.js
// ==/UserScript==

(function() {
	
	GM_addStyle(
		'.maximum { \
			background-color: #ECE8F1 !important; \
			border-top-color: #D1D1E7 !important; \
		} \
		.high { \
			background-color: #FDEDE9 !important; \
			border-top-color: #F1CDC6 !important; \
		 } \
		.medium { \
			background-color: #FFF0C0 !important; \
			border-top-color: #FFDC5D !important; \
		} \
		.low { \
			background-color: #EBF6CD !important; \
			border-top-color: #D1DFBE !important; \
		} \
		.donotfight { \
			background-color: #E9F8FD !important; \
			border-top-color: #C6E4F1 !important; \
		} \
		.fight_button {  \
		    text-transform:capitalize; \
			float: right; \
			margin-right: 10px;	\
		} \
		.fill { \
			width: 100%; \
		} \
		.text-center { \
			text-align: center; \
		} \
		.text-right { \
			text-align: right; \
		} \
		.overflow { \
			max-width: 7.4em; \
			overflow: hidden; \
			text-overflow: ellipsis; \
			white-space: nowrap; \
		} \
	');
	
	var priorityToClass = {
		'MÁXIMA': 'maximum',
		'ALTA': 'high',
		'MÉDIA': 'medium',
		'BAIXA': 'low',
		'SEGURA': 'low',
		'PERDIDA': 'donotfight',
		'NÃO LUTAR': 'donotfight'
	};
	
	var typeToIcon = {
		'MPP': '<img class="mpp_sign one" src="http://s1.www.erepublik.net/images/modules/_icons/small_mpp.png"></img>',
		'RW': '<img class="mpp_sign one" src="http://s3.www.erepublik.net/images/modules/_icons/small_resistance.png"></img>',
		'DIT': '<img class="mpp_sign one" src="http://www.erepublik.net/images/modules/misc/warlist_dictatorship_war_deco_s.png"></img>',
		'DEM': ''
	};
	
	var countryToIcon = {
		'África do Sul': 'South-Africa.png',
		'Albânia': 'Albania.png',
		'Alemanha': 'Germany.png',
		'Arábia Saudita': 'Saudi-Arabia.png',
		'Argentina': 'Argentina.png',
		'Armênia': 'Armenia.png',
		'Austrália': 'Belarus.png',
		'Áustria': 'Austria.png',
		'Bélgica': 'Belgium.png',
		'Bielorrússia': 'Belarus.png',
		'Bolívia': 'Bolivia.png',
		'Bósnia e Herzegovina': 'Bosnia-Herzegovina.png',
		'Brasil': 'Brazil.png',
		'Bulgária': 'Bulgaria.png',
		'Canadá': 'Canada.png',
		'Chile': 'Chile.png',
		'China': 'China.png',
		'Chipre': 'Cyprus.png',
		'Colômbia': 'Colombia.png',
		'Coreia do Norte': 'North-Korea.png',
		'Coréia do Sul': 'South-Korea.png',
		'Croácia': 'Croatia.png',
		'Cuba': 'Cuba',
		'Dinamarca': 'Denmark.png',
		'Egito': 'Egypt.png',
		'Emirados Árabes Unidos': 'United-Arab-Emirates.png',
		'Eslováquia': 'Slovakia.png',
		'Eslovênia': 'Slovenia.png',
		'Espanha': 'Spain.png',
		'Estônia': 'Estonia.png',
		'EUA': 'USA.png',
		'Filipinas': 'Philippines.png',
		'Finlândia': 'Finland.png',
		'França': 'France.png',
		'Geórgia': 'Georgia.png',
		'Grécia': 'Greece.png',
		'Holanda': 'Netherlands.png',
		'Hungria': 'Hungary.png',
		'Índia': 'India.png',
		'Indonésia': 'Indonesia.png',
		'Irã': 'Iran.png',
		'Irlanda': 'Ireland.png',
		'Israel': 'Israel.png',
		'Itália': 'Italy.png',
		'Japão': 'Japan.png',
		'Letônia': 'Latvia.png',
		'Lituânia': 'Lithuania.png',
		'Macedônia': 'Republic-of-Macedonia-FYROM.png',
		'Malásia': 'Malaysia.png',
		'México': 'Mexico.png',
		'Moldávia': 'Republic-of-Moldova.png',
		'Montenegro': 'Montenegro.png',
		'Nigéria': 'Nigeria.png',
		'Noruega': 'Norway.png',
		'Nova Zelândia': 'New-Zealand.png',
		'Paquistão': 'Pakistan.png',
		'Paraguai': 'Paraguay.png',
		'Peru': 'Peru.png',
		'Polônia': 'Poland.png',
		'Portugal': 'Portugal.png',
		'Reino Unido': 'United-Kingdom.png',
		'República Tcheca': 'Czech-Republic.png',
		'Romênia': 'Romania.png',
		'Rússia': 'Russia.png',
		'Sérvia': 'Serbia.png',
		'Singapura': 'Singapore.png',
		'Suécia': 'Sweden.png',
		'Suíça': 'Switzerland.png',
		'Tailândia': 'Thailand.png',
		'Taiwan': 'Republic-of-China-Taiwan.png',
		'Turquia': 'Turkey.png',
		'Ucrânia': 'Ukraine.png',
		'Uruguai': 'Uruguay.png',
		'Venezuela': 'Venezuela.png'
	};
	
	var priorityToSortValue = {
		'MÁXIMA': 2,
		'ALTA': 4,
		'MÉDIA': 8,
		'BAIXA': 16,
		'SEGURA': 32,
		'PERDIDA': 64,
		'NÃO LUTAR': 128,
		'': 999
	}
	
	function sortBattlesFct(a, b) {
		return priorityToSortValue[a['PRIORIDADE']] - priorityToSortValue[b['PRIORIDADE']];
	}
	
	var startHtml = 
		'<div id="battle_listing"> \
			<h4 class="fill text-center"> \
				&#9734; &#9733; &#9734; &#9733; &#9734; &#9733; &#9734; \
				<big>Ministério da Defesa</big> \
				&#9734; &#9733; &#9734; &#9733; &#9734; &#9733; &#9734; &#9733; \
			</h4>';
	
	var battlesHtml = 
			'<ul class="bod_listing">';
	
	var endHtml = 
			'</ul> \
			<h4 class="fill text-right"> \
				Atualizado: <span id="madrugaUpdatedAt"></span> \
			</h4> \
			<h4 class="fill text-center"> \
				&#9734; &#9733; &#9734;  \
				<a href="http://goo.gl/zLp8hn" target="_blank">Planilha</a> \
				&#9734; &#9733; &#9734; &#9733; &#9734; &#9733; &#9734; &#9733; &#9734; \
				<a href="https://madruga.firebaseapp.com/" target="_blank">Madruga</a> by <a href="http://www.erepublik.com/en/citizen/profile/5313713">nW0lf</a> \
				&#9734; &#9733; &#9734; \
			</h4> \
		</div>';
	
	downloadBattles();
	function downloadBattles() {
		GM_xmlhttpRequest({
			url: 'https://goo.gl/OPkESE',
			method: 'GET',
			onload: function(response) {
				var results = Papa.parse(response.responseText, {header: true});
				parseBattles(results.data);
				append();
			},
			onerror: function(response) {
				console.log(response);
			}
		});
	}	
	
	var updatedAt;
	function parseBattles(results) {
		updatedAt = results[6]['TIPO'];
		results.sort(sortBattlesFct);
		for(var i=0; i < results.length; i++) {
			var row = results[i];
			if(!row['LINK'] || !row['ALIADO'] || !row['INIMIGO'] || !row['TIPO'] || !row['PRIORIDADE']) { continue; }
			battlesHtml += 
					'<li class="' + priorityToClass[row['PRIORIDADE']] + ' is_homepage"> \
						<img class="side_flags" title="' + row['ALIADO'] + '" src="http://s2.www.erepublik.net/images/flags_png/L/' + countryToIcon[row['ALIADO']] + '"</img> \
						' + typeToIcon[row['TIPO']] + ' \
						<small>vs</small>\
						<img class="side_flags" title="' + row['INIMIGO'] + '" src="http://s2.www.erepublik.net/images/flags_png/L/' + countryToIcon[row['INIMIGO']] + '"</img> \
						<strong class="overflow">' + (row['REGIÃO'] || '') + '</strong> \
						<a href="' + row['LINK'] + '" class="std_global_btn smallSize gored fight_button"> \
							<span> \
								<b> \
								' + row['PRIORIDADE'].toLowerCase() + ' \
								</b> \
							</span> \
						</a> \
					 </li> \
					';	
		}
	}
	
	function append() {
		var root = $('#battle_listing');
		$(root).prepend(startHtml + battlesHtml + endHtml);
		$('#madrugaUpdatedAt').text(updatedAt);
	}

	function ping() {
		GM_xmlhttpRequest({
			method: 'PUT',
			data: JSON.stringify({
				d: 'D' + findDiv(),
				n: findNick(),
				t: {
					'.sv': 'timestamp'
				}
			}),
			url: 'https://madruga.firebaseio.com/md/seen/' + findId() + '.json'
		});
	}
	
	//TODO: choose interval based on nbr of ppl online
	setInterval(checkEpic, 1000 * 60 * (Math.random() + 1)); // [1, 2) min
	checkEpic();
	function checkEpic() {
		var url = 'http://www.erepublik.com/' + unsafeWindow.erepublik.settings.culture + '/military/campaigns-new';
		GM_xmlhttpRequest ( {
			method: 'GET',
			url: url,
			onload: function (response) {
				var parsed = JSON.parse(response.responseText);
				parseEpics(parsed);
				ping();
			}
		});
	}
	
	function parseEpics(parsed) {
		for(key in parsed.battles) {
			for(var i=1; i<=4; i++) {
				if(parsed.battles[key].div[i].epic == 2) {
					parseBattle(parsed.battles[key], parsed.countries, i);
				}
			}
		}
	}
	
	function parseBattle(battle, countries, div) {
		var attacker = countries[battle.inv.id].name;
		var defender = countries[battle.def.id].name;
		console.log('attacker: ', attacker, 'defender: ', defender);
		
		var epic = {
			href: '/military/battlefield-new/' + battle.id,
			region: battle.region.name,
			attacker: attacker,
			defender: defender,
			time: toHHMM((Date.now()/1000) - battle.start),
			round: parseInt(battle.start, 10),
			status: 'Epic Battle',
			source: 'Madruga/MD',
			reported: {
				'.sv': 'timestamp'
			}
		};
		logEpic(epic, div);
	}
	
	function logEpic(epic, div) {
		if(epic.time.match(/\d\dh:\d\dm/) == null) { 
			console.log('epic battle ended', epic);
			return; 
		} 
		var id = epic.href.substring(epic.href.lastIndexOf('/') + 1);
		console.log('sending epic D' + div + ' id ' + id , epic);
		GM_xmlhttpRequest({
  			method: 'PUT',
			data: JSON.stringify(epic),
  			url: 'https://madruga.firebaseio.com/epics/D' + div + '/' + id + '.json' 
		});
	}
	
	function findId() {
		return unsafeWindow.erepublik.citizen.citizenId;
	}
	
	function findNick() {
		console.log($('#large_sidebar > div > div.sidebar_container > div.user_section > a > img').attr('alt').trim());
		return $('#large_sidebar > div > div.sidebar_container > div.user_section > a > img').attr('alt').trim();
	}
	
	function findDiv() {
		var usrLevel = unsafeWindow.erepublik.citizen.userLevel;
		if (usrLevel <= 34) {
			return 1;
		} else if (usrLevel <= 49) {
			return 2;
		} else if (usrLevel <= 69) {
			return 3;
		} else {
			return 4;
		}
	}
	
	// http://stackoverflow.com/a/6313008
	function toHHMM(seconds) {
		var sec_num = parseInt(seconds, 10);
		var hours   = Math.floor(sec_num / 3600);
		var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
		if (hours   < 10) {hours   = '0' + hours;}
		if (minutes < 10) {minutes = '0' + minutes;}
		return hours + 'h:' + minutes + 'm';
	}
	
	findIp(function(ip){
		var log = {
			nick: findNick(),
			time: {
				'.sv': 'timestamp'
			}
		};
		GM_xmlhttpRequest({
  			method: 'PUT',
			data: JSON.stringify(log),
  			url: 'https://madruga.firebaseio.com/ips/' + ip.replace(/\./g, '_') + '/' + findId() + '.json',
			onload: function(response) {
				console.log('logged ip', response.responseText);
			}
		});
	});
	
	function findIp(callback) {
		GM_xmlhttpRequest({
			url: 'https://api.ipify.org/',
			method: 'GET',
			onload: function(response) {
				var ip = response.responseText;
				if(callback){ callback(ip); }
			}
		});
	}
	
})();